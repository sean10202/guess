package com.home.guess

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val secretNumber = SecretNumber()
    val TAG: String = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(TAG, "secret number: " + secretNumber.secret.toString())
    }

    fun check (view: View) {
        val pick = ed_number.text.toString().toInt()
        val compare = secretNumber.Validate(pick)
        val count = secretNumber.count
        var message: String = ""

        if(compare < 0) {
            message = getString(R.string.bigger)
        } else if (compare > 0) {
            message = getString(R.string.smaller)
        } else if (compare == 0 && count >= 3) {
            message = getString(R.string.correct)
        }else {
            message = getString(R.string.excellent) + secretNumber.secret
        }
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.bingo))
            .setMessage(message)
            .setPositiveButton(getString(R.string.ok), null)
            .show()
    }
}
