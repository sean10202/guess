package com.home.guess

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.ed_number

import kotlinx.android.synthetic.main.activity_material.*
import kotlinx.android.synthetic.main.content_material.*

class MaterialActivity : AppCompatActivity() {
    private val REQUEST_REPLAY: Int = 100
    val secretNumber = SecretNumber()
    val TAG: String = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_material)
        setSupportActionBar(toolbar)
        Log.d(TAG, "secret number: " + secretNumber.secret.toString())

        fab.setOnClickListener { view ->
            replay()
        }
    }

    private fun replay() {
        AlertDialog.Builder(this)
            .setTitle("Replay game")
            .setMessage("Are you sure?")
            .setPositiveButton(getString(R.string.ok), { Dialog, which ->
                secretNumber.reset()
                counter.setText(secretNumber.count.toString())
                ed_number.setText("")
                Log.d(TAG, "secret number: " + secretNumber.secret.toString())
            })
            .setNeutralButton("leave", { Dialog, which -> finish() })
            .setNegativeButton("Cancel", null)
            .show()
    }

    fun check (view: View) {
        val pick = ed_number.text.toString().toInt()
        val compare = secretNumber.Validate(pick)
        val message: String

        message = when(compare) {
            0 -> if(secretNumber.count >= 3) {
                getString(R.string.correct)
            } else {
                getString(R.string.excellent) + secretNumber.secret
            }
            else -> {
                if(compare <0) {
                    getString(R.string.bigger)
                } else {
                    getString(R.string.smaller)
                }
            }
        }

        /*message = if(compare < 0) {
            getString(R.string.bigger)
        } else if (compare > 0) {
            getString(R.string.smaller)
        } else if (compare == 0 && secretNumber.count >= 3) {
            getString(R.string.correct)
        }else {
            getString(R.string.excellent) + secretNumber.secret
        }*/
        ed_number.setText("")
        counter.setText(secretNumber.count.toString())
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.bingo))
            .setMessage(message)
            .setPositiveButton(getString(R.string.ok), { Dialog, which ->
                if (compare == 0) {
                    val intent = Intent(this, RecordActivity::class.java)
                    intent.putExtra("counter", secretNumber.count)
                    startActivityForResult(intent, REQUEST_REPLAY)
                }
            }).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_REPLAY) {
            if(resultCode == Activity.RESULT_OK) {
                val nickname = data?.getStringExtra("NICKNAME")
                Log.d(TAG, nickname)
                replay()
            }
        }
    }
}
