package com.home.guess

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_record.*

class RecordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record)

        val counter = intent.getIntExtra("counter", 0)
        show_counter.setText(counter.toString())

        save.setOnClickListener { view ->
            val nick = nick_name.text.toString()
            getSharedPreferences("guess", Context.MODE_PRIVATE)
                .edit()
                .putString("REC_NICKNAME", nick)
                .putInt("REC_COUNTER", counter)
                .apply()
            val intent = intent.putExtra("NICKNAME", nick)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
}
